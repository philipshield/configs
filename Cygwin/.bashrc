# base-files version 4.1-1
# ~/.bashrc: executed by bash(1) for interactive shells.
# The latest version as installed by the Cygwin Setup program can
# always be found at /etc/defaults/etc/skel/.bashrc

# If not running interactively, don't do anything
[[ "$-" != *i* ]] && return

PATH="${PATH}:."
# export PS1="\[\e]0;\w\a\]\[\e[32m\]\u@\h \[\e[33m\]\w>\[\e[0m\] "
export PS1='\[\e]0;\w\a\]\[\e[0;35m\]\u@\h \[\e[33m\]\w$(__git_ps1)\[\e[0;35m\]>\[\e[0m\] '

# Don't wait for job termination notification
# set -o notify

# Don't use ^D to exit
# set -o ignoreeof

# Use case-insensitive filename globbing
shopt -s nocaseglob

# When changing directory small typos can be ignored by bash
# for example, cd /vr/lgo/apaache would find /var/log/apache
# shopt -s cdspell

alias less='less -r'                          # raw control characters
alias whence='type -a'                        # where, of a sort
alias grep='grep --color'                     # show differences in colour
alias egrep='egrep --color=auto'              # show differences in colour
alias fgrep='fgrep --color=auto'              # show differences in colour
alias ls='ls -hF --group-directories-first --color=tty --show-control-chars'    # classify files in colour
alias ll='ls -g'                              # long list
alias la='ll -A'                              # all but . and ..
alias tree='tree -C --dirsfirst'
alias cd..='cd ..'
alias ..='cd ..'
alias cd-='cd -'
alias submit='~/Dropbox/Code/Config/submit.py'
alias kattisdir='cd ~/Dropbox/Code/Kattis/'
alias codedir='cd ~/Dropbox/Code/'

function explore() { explorer .; }

bygg() { eval "g++ -std=gnu++0x -Wall -O2 -o $1.exe $* 2>&1 | grep -v mingw32"; }
vs() { eval "/cygdrive/c/Program\ Files\ \(x86\)/Microsoft\ Visual\ Studio\ 11.0/Common7/ide/devenv.exe $1 &"; }
csc() { eval "/cygdrive/c/Windows/Microsoft.NET/Framework64/v4.0.30319/csc.exe $1"; }
upload() { eval "scp $1 Administrator@nikal.se:~/Niklas/htdocs/upload/"; }
uploadl() { eval "cp --interactive --verbose --target-directory=/cygdrive/y/htdocs/upload/ $1"; }

alias st='git status'
function pupdate()
{
  git submodule sync
  git submodule update --init --recursive
}
function pbranch()
{
  git branch nikal/$1
  git checkout nikal/$1
  git push origin HEAD:nikal/$1
  git push --set-upstream origin nikal/$1
}
function punbranch()
{
  git push origin :nikal/$1
  git branch -D nikal/$1
}
function pgrep()
{
  grep -inr --color=auto "$*" .
}
function plogtest()
{
  git log --all --date-order --date=relative --graph --decorate=short --pretty=format:"%h %an %ar \"%s\"";
}
function plog()
{
  git log --all --date-order --oneline --date=relative --graph --decorate=short;
}
function plog2()
{
  git log --all --date-order --abbrev-commit --pretty=short --date=relative --graph --decorate=short;
}
function plog3()
{
  git log --all --date-order --pretty=fuller --date=relative --graph --decorate=short;
}
